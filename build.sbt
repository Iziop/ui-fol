 lazy val root = project
  .in(file("."))
  .enablePlugins(ScalaJSBundlerPlugin)
  .settings(
    organization := "ru.mephi.csit",
    name := "uifier",
    scalaVersion := "2.13.5",
    resolvers += "jitpack" at "https://jitpack.io",
    libraryDependencies ++= Seq(
      "org.typelevel" %%% "simulacrum" % "1.0.0",
      "com.github.outwatch.outwatch" %%% "outwatch" % "master-ff9af0b0f7-1",
      "com.lihaoyi" %%% "fastparse" % "2.3.2",
      "org.scalatest" %%% "scalatest" % "3.2.2" % "test",
      "com.github.scopt" %%% "scopt" % "4.0.0-RC2",
      "io.monix" %%% "monix" % "3.4.0"
    ),
    scalafmtOnCompile := !insideCI.value,
    scalacOptions ++= Seq(
      "-Ymacro-annotations",
      "-feature",
    ),
    webpackBundlingMode := BundlingMode.LibraryAndApplication(),
    scalaJSUseMainModuleInitializer := true,
    mainClass in Compile := Some("ru.mephi.csit.uifier.UifierIOApp"),
  )

val openDev =
  taskKey[Unit]("open index-dev.html")

openDev := {
  val url = baseDirectory.value / "index-dev.html"
  streams.value.log.info(s"Opening $url in browser...")
  java.awt.Desktop.getDesktop.browse(url.toURI)
}
