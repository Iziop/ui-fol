package ru.mephi.csit.uifier

import cats.effect.SyncIO
import outwatch._
import outwatch.dsl._
import outwatch.reactive.handler.Handler
import Router._

final private class glav(pageHndl: Handler[Page]) {

  def listGroupItem(name: String, page: Page) =
    div(
      cls := "back-btn",
      button(
        cls := "back-btn",
        name,
        onClick.use(page) --> pageHndl,
      ),
    )

  val nodeback = SyncIO(
    div(
      listGroupItem("Главная", FirstPage),
    ),
  )
}
