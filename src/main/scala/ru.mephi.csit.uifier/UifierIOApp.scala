package ru.mephi.csit.uifier

import cats.effect.{ExitCode, IO, IOApp}
import outwatch._
import outwatch.dsl._

object UifierIOApp extends IOApp {

  def run(args: List[String]): IO[ExitCode] =
    for {
      demo <- ResultComponent.init
      _ <- OutWatch.renderReplace[IO](
        "#main",
        div(demo.node, styles.height := "100%"),
      )
    } yield ExitCode.Success

}
