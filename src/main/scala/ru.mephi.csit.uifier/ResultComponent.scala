package ru.mephi.csit.uifier

import cats.effect.IO
import outwatch._
import outwatch.dsl._
import outwatch.reactive.handler.Handler
import Router._
import ResultComponent._

final case class ResultComponent(
  firstPage: VNode,
  demoComponent: DemoComponent,
  compare: Compare,
  is_normal: Is_normal,
  glavnaya: VNode,
) {

  def node = VDomModifier(
    div(
      cls := "d-flex",
      div(
        cls := "page-content",
        idAttr := "content",
        glavnaya,
        pgeHndl.map {
          case IsNormalPage => is_normal.node
          case ComparePage  => compare.node
          case SolutionPage => demoComponent.node
          case FirstPage    => firstPage

        },
      ),
    ),
  )
}

object ResultComponent {
  val pgeHndl = Handler.unsafe[Page](FirstPage)
  def init: IO[ResultComponent] = (for {
    isnormal  <- Is_normal.init
    compare   <- Compare.init
    solution  <- DemoComponent.init
    firstpage <- new FirstPage(pgeHndl).node
    glav      <- new glav(pgeHndl).nodeback

  } yield
    ResultComponent(
      firstpage,
      solution,
      compare,
      isnormal,
      glav,
    )).toIO
}
