package ru.mephi.csit.uifier

object Router {
  sealed trait Page
  case object IsNormalPage extends Page
  case object SolutionPage extends Page
  case object ComparePage extends Page

  case object FirstPage extends Page
}
