package ru.mephi.csit.uifier

import cats.effect.SyncIO
import outwatch._
import outwatch.dsl._
import outwatch.reactive.handler.Handler
import Router._

final private class FirstPage(pageHandler: Handler[Page]) {
  var pageStream: Handler[Page] = pageHandler

  def node = SyncIO(
    div(
      cls := "d-flex flex-column col-9 min-vh-600",
      header(
        cls := "row d-flex justify-content-center subsystem-title",
        marginBottom := "1.14rem",
        h1("Приведение к ПНФ"),
      ),
      div(
        cls := "container",
        div(
          cls := "row",
          a(
            cls := "col-md-12",
            href := "#",
            div(
              cls := "card mb-4 box-shadow task-title",
              "Задание на приведение к ПНФ",
            ),
            onClick(IsNormalPage) --> pageHandler,
          ),
          a(
            cls := "col-md-12",
            href := "#",
            div(
              cls := "card mb-4 box-shadow task-title",
              "Решение задания на приведения к ПНФ",
            ),
            onClick(SolutionPage) --> pageHandler,
          ),
          a(
            cls := "col-md-12",
            href := "#",
            div(
              cls := "card mb-4 box-shadow task-title",
              "Задание на сравнение формул",
            ),
            onClick(ComparePage) --> pageHandler,
          ),
        ),
      ),
    ),
  )
}
