package ru.mephi.csit.uifier

import cats.data.ValidatedNel
import cats.syntax.apply._
import colibri.Observable
import fastparse.Parsed
import outwatch.VDomModifier
import outwatch.dsl._
import ru.mephi.csit.uifier.Editable.ops._
import fastparse.Parsed
import scopt.OParser

final case class Person(firstName: String)

object Person {

  val personPresentable: Presentable[Person] =
    Presentable(person => span(s"${person.firstName} ${person.firstName}"))

  implicit val personEditable: Editable[Person] =
    person =>
      for {
        firstNameEditor <- person.firstName.editor

      } yield
        new Editor[Person] {

          def result: Observable[ValidatedNel[String, Person]] =
            firstNameEditor.result.map(
              _.map {
                var res: String = ""

                x =>
                  val parse_result: Parsed[Formula] = FormulaParser.Parse(x)
                  parse_result match {
                    case Parsed.Success(value, _) =>
                      res = " is " + IndexedFormula
                        .toPrenex(Formula.toIndexed(value)._1)

                    case failure: Parsed.Failure =>
                      res = "Incorrect formula, error:\n" + failure.trace()
                  }

                  Person("Prenex of " + x + res)

              },
            )

          """def result: Observable[ValidatedNel[String, Person]] =
            firstNameEditor.result.map(
              _.map(x => Person("Prenex of " + x + res)),
            )"""
          def ispnf(for1: String): Observable[ValidatedNel[String, Person]] =
            firstNameEditor.result.map(
              _.map {
                var res: String = ""

                x =>
                  val p = FormulaParser.Parse(for1)
                  p match {
                    case Parsed.Success(value, _) =>
                      val pnf = IndexedFormula
                        .toPrenex(Formula.toIndexed(value)._1)
                      val cond = pnf.toString == x
                      if (cond) res = "Right!"
                      else res = "Wrong!"

                    case failure: Parsed.Failure =>
                      res = "Incorrect formula, error:\n" + failure.trace()
                  }
                  Person(res)

              },
            )
          def present: VDomModifier =
            div(
              div("Формула:", firstNameEditor.present),
            )

          override def isnormal2: Observable[ValidatedNel[String, Person]] = ???
        }
}
