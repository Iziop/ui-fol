package ru.mephi.csit.uifier

import Editable.ops._
import cats.effect.SyncIO
import outwatch.{HtmlVNode, VDomModifier, VNode}
import outwatch.dsl._
import outwatch.reactive.handler.Handler
import ru.mephi.csit.uifier.ResultComponent.pgeHndl
import ru.mephi.csit.uifier.Router.{FirstPage, IsNormalPage, Page}

final class Is_normal(editor: Editor[Person]) {

  val nodeforbutton: SyncIO[VNode] = for {
    goHandler <- Handler.create[Boolean]

  } yield
    div(
      cls := "row d-flex flex-column col-9 ml-2 nizbtn",
      button(
        "Получить случайную формулу",
        idAttr := "reductionButton",
        `type` := "submit",
        color := "#000000",
        cls := "btn btn-lg btn-secondary col",
        onClick.use(true) --> goHandler,
      ),
      goHandler.map(if (_) {
        val formula =
          ru.mephi.csit.uifier.Formula.randomFormula(3, 4, 4).toString
        VDomModifier(
          div(
            cls := "niz",
            formula,
            h1("Ваш ответ:"),
            editor.present,
            h1("Результат:"),
            div(
              editor
                .ispnf(formula)
                .map(_.map(person => person.firstName)).map(
                  _.getOrElse("∀r.∃f.∀v.R(v)"),
                ),
            ),
          ),
        )
      } else div()),
    )

  val node: HtmlVNode =
    div(
      h1("Получите формулу, приведите в ПНФ и введите ваш ответ"),
      nodeforbutton,
    )

}

object Is_normal {
  def init: SyncIO[Is_normal] =
    for {
      editor <- Person("∀r.∃f.∀v.R(v)").editor
    } yield new Is_normal(editor)
}
