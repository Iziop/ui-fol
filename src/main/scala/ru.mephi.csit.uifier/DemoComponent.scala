package ru.mephi.csit.uifier

import Editable.ops._
import cats.effect.SyncIO
import outwatch._
import outwatch.{HtmlVNode, VDomModifier}
import outwatch.dsl._
import outwatch.reactive.handler.Handler
import ru.mephi.csit.uifier.Router.FirstPage

final class DemoComponent(editor: Editor[Person]) {

  val nodeforbutton: SyncIO[VNode] = for {
    goHandler <- Handler.create[Boolean]

  } yield
    div(
      cls := "row d-flex flex-column col-9 ml-2 nizbtn",
      button(
        "Сгенерировать случайную формулу",
        idAttr := "reductionButton",
        `type` := "submit",
        color := "#000000",
        cls := "btn btn-lg btn-secondary col",
        onClick.use(true) --> goHandler,
      ),
      goHandler.map(if (_) {

        VDomModifier(
          div(
            cls := "niz",
            ru.mephi.csit.uifier.Formula.randomFormula(3, 4, 4).toString,
          ),
        )
      } else div()),
    )

  val node: HtmlVNode =
    div(
      cls := "row d-flex flex-column col-9 ml-2",
      h1("Ввод:"),
      editor.present,
      h1("Результат:"),
      div(
        editor.result
          .map(_.map(person => person.firstName)).map(_.getOrElse("")),
      ),
      div(nodeforbutton),
    )

}

object DemoComponent {
  def init: SyncIO[DemoComponent] =
    for {
      editor <- Person("∀r.∃f.∀v.R(v)").editor
    } yield new DemoComponent(editor)
}
