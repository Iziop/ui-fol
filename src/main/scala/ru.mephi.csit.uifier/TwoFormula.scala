package ru.mephi.csit.uifier

import colibri.Subject
import cats.data.ValidatedNel
import cats.effect.SyncIO
import cats.syntax.apply._
import colibri.Observer.sink
import colibri.{Observable, Sink}
import fastparse.Parsed
import outwatch.{VDomModifier, VNode}
import outwatch.dsl._
import outwatch.reactive.handler.Handler
import ru.mephi.csit.uifier.Editable.ops._
import ru.mephi.csit.uifier.IndexedFormula.toPrenex
import monix.execution.Scheduler.Implicits.global

final case class TwoFormula(firstFormula: String, secondFormula: String)

object TwoFormula {

  val twoformulaPresentable: Presentable[TwoFormula] =
    Presentable(twoformula =>
      span(s"${twoformula.firstFormula} ${twoformula.secondFormula}"),
    )

  implicit val twoformulaEditable: Editable[TwoFormula] =
    twoformula =>
      for {
        firstNameEditor <- twoformula.firstFormula.editor
        lastNameEditor  <- twoformula.secondFormula.editor
      } yield
        new Editor[TwoFormula] {

          def result: Observable[ValidatedNel[String, TwoFormula]] =
            Observable.combineLatestMap(
              firstNameEditor.result,
              lastNameEditor.result,
            )(
              (
                firstNameValidated,
                lastNameValidated,
              ) =>
                (firstNameValidated, lastNameValidated).mapN {
                  var res: String = ""
                  (f1, f2) =>
                    val (l, r) = (
                      FormulaParser.Parse(f1),
                      FormulaParser.Parse(f2),
                    )
                    l match {
                      case Parsed.Success(v1, _) =>
                        r match {
                          case Parsed.Success(v2, _) =>
                            if (v1 == v2) res = f1 + " and " + f2 + " are same!"
                            else {
                              val p1 = toPrenex(Formula.toIndexed(v1)._1)
                              val p2 = toPrenex(Formula.toIndexed(v2)._1)
                              var bool = ""
                              if (p1 == p2) bool = "true" else bool = "false"
                              res = f1 + " == " + f2 + " is " + bool
                            }

                          case failure: Parsed.Failure =>
                            res =
                              "Incorrect formula, error:\n" + failure.trace()

                        }
                      case failure: Parsed.Failure =>
                        res = "Incorrect formula, error:\n" + failure.trace()

                    }
                    TwoFormula.apply(res, "")

                },
            )
          def isnormal2: Observable[ValidatedNel[String, TwoFormula]] =
            Observable.combineLatestMap(
              firstNameEditor.result,
              lastNameEditor.result,
            )(
              (
                firstNameValidated,
                lastNameValidated,
              ) =>
                (firstNameValidated, lastNameValidated).mapN(TwoFormula.apply),
            )

          def present: VDomModifier =
            div(
              div("Формула 1:", firstNameEditor.present),
              div("Формула 2:", lastNameEditor.present),
            )

          override def ispnf(
            x: String,
          ): Observable[ValidatedNel[String, TwoFormula]] = ???
        }

}
