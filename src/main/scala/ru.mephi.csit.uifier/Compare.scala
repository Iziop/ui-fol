package ru.mephi.csit.uifier

import colibri.Subject
import Editable.ops._
import cats.effect.SyncIO
import outwatch.{HtmlVNode, VDomModifier, VNode}
import outwatch.dsl._
import outwatch.reactive.handler.Handler

final class Compare(editor: Editor[TwoFormula]) {
  val counter = Subject.behavior("")
  val node: HtmlVNode =
    div(
      h2(
        "Введите формулу 2 равносильной формуле 1, чтобы результат показывал true",
      ),
      h1("Редактор:"),
      editor.present,
      h1("Результат:"),
      div(
        editor.result
          .map(_.map(formulas => formulas.firstFormula)).map(_.getOrElse("")),
      ),
    )
}

object Compare {
  def init: SyncIO[Compare] =
    for {
      editor <- TwoFormula(
        ru.mephi.csit.uifier.Formula.randomFormula(3, 4, 4).toString,
        ru.mephi.csit.uifier.Formula.randomFormula(3, 4, 4).toString,
      ).editor
    } yield new Compare(editor)
}
